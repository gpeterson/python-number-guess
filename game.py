from random import randint

# Python 3 check.
try:
    read_from_console = raw_input
except NameError:
    read_from_console = input


class NumberGuess(object):
    ''' Generic number guesser.

        If a random value is not supplied it will be generated.

        This also provides some hooks to override for subclasses.

        :param int value: The "random" value.
        :param int minimum: The min range bound. Defaults to 0.
        :param int maximum: The max range bound. Defaults to 100.
        :param int max_chances: The max number of chances. Defaults to 7.
    '''

    def __init__(self, value=None, minimum=0, maximum=100, max_chances=7):
        self.value = value
        self.minimum = minimum
        self.maximum = maximum
        self.max_chances = max_chances

        if self.minimum >= self.maximum:
            raise ValueError('minimum should be less than maximum')

        self.reset_game(value)

    def reset_game(self, value=None):
        ''' Reset game state.

            :param int value: Optional value for testing.
        '''

        self.current_turn = 1

        if not value:
            value = randint(self.minimum, self.maximum)
        self.value = value

    def get_number(self):
        ''' Override this to dynamically update the number. '''

        raise NotImplemented()

    def display_error_message(self, msg):
        ''' Display an error message.

            :param str msg: The error message.
            :return: A formatted message.
            :rtype: str
        '''

        return "Incorrect {}".format(msg)

    def display_success_message(self):
        ''' Display a success message.

            :return: A formatted message.
            :rtype: str
        '''

        return "Congratulations the value was {}".format(self.value)

    @property
    def remaining_chances(self):
        ''' Returns the remaining chances. '''

        diff = self.max_chances - self.current_turn
        if diff < 0:
            diff = 0

        return diff

    def display_remaining_chances(self):
        ''' Display remaining chances message.

            :return: A formatted message.
            :rtype: str
        '''

        return "You have {} guesses left".format(self.remaining_chances)

    def display_correct_value(self):
        ''' Display the correct value message.

            Inteded to happen at the end of the game.

            :return: A formatted message.
            :rtype: str
        '''

        return "The number was {}".format(self.value)

    def validate_input(self, guess):
        ''' Validates that guess is a number in the correct range.

            :param str guess: The guess made.
            :returns: A tuple of (success, guess, errors).
            :rtype: tuple.
        '''

        try:
            guess = int(guess)
        except ValueError:
            return (False, guess, 'Please enter a number.')

        if guess < self.minimum or guess > self.maximum:
            msg = ('Please enter a number between {} and {}'
                   .format(self.minimum, self.maximum))
            return (False, guess, msg)

        return (True, guess, '')

    def check_guess(self, guess):
        ''' Returns a message indicating whether the guess matches the value.

            :param int guess: The guess made.
            :returns: A tuple of (keep_going, message)
            :rtype: tuple
        '''

        msg = ''
        keep_going = True
        if guess == self.value:
            msg = "Congratulations the value was {}".format(self.value)
            keep_going = False
        elif guess < self.value:
            msg = 'Guess was too low.'
        else:
            msg = 'Guess was too high.'

        return keep_going, msg

    def run_turn(self, guess):
        ''' Run a single turn.

            :param str guess: The guess made.
        '''

        if self.current_turn > self.max_chances:
            self.display_correct_value()
            return False

        keep_going, msg = self.check_guess(guess)

        if not keep_going:
            self.display_success_message()
            return False

        self.display_error_message(msg)
        self.display_remaining_chances()

        self.current_turn += 1

        return keep_going

    def run(self):
        ''' Helper to run the game. '''

        self.reset_game()

        keep_going = True

        while keep_going:
            guess = self.get_number()
            keep_going = self.run_turn(guess)


class ConsoleNumberGuess(NumberGuess):
    ''' Subclass to read from standard input. '''

    def get_number(self):
        ''' Get a number from standard input. '''

        success = False
        while not success:
            guess = read_from_console('Please enter a guess: ')

            success, guess, error = self.validate_input(guess)
            if not success:
                print(error)

        return guess

    def display_error_message(self, msg):
        ''' Overriden to specifically print message to screen. '''

        print(super(ConsoleNumberGuess, self).display_error_message(msg))

    def display_success_message(self):
        ''' Overriden to specifically print message to screen. '''

        print(super(ConsoleNumberGuess, self).display_success_message())

    def display_remaining_chances(self):
        ''' Overriden to specifically print message to screen. '''

        print(super(ConsoleNumberGuess, self).display_remaining_chances())

    def display_correct_value(self):
        ''' Overriden to specifically print message to screen. '''

        print(super(ConsoleNumberGuess, self).display_correct_value())
