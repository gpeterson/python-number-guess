import unittest
from game import NumberGuess


class TestValidateInput(unittest.TestCase):

    def setUp(self):
        self.value = 50
        self.minimum = 0
        self.maximum = 100

    def test_min_greater_than_max(self):
        minimum = 100
        maximum = 0

        with self.assertRaises(ValueError):
            NumberGuess(minimum=minimum, maximum=maximum)

    def test_min_equal_to_max(self):
        minimum = 50
        maximum = 50

        with self.assertRaises(ValueError):
            NumberGuess(minimum=minimum, maximum=maximum)

    def test_valid(self):
        guess = '50'
        value = 50
        ng = NumberGuess(value)
        success, value, error = ng.validate_input(guess)

        assert success is True
        assert value == 50
        assert error == ''

    def test_non_number(self):
        guess = 'abc'
        ng = NumberGuess(self.value)
        success, value, error = ng.validate_input(guess)

        assert success is False
        assert value == 'abc'
        expected_error = 'Please enter a number.'
        assert error == expected_error

    def test_lower_bounds(self):
        minimum = 10
        value = '1'
        ng = NumberGuess(value, minimum=minimum)
        success, value, error = ng.validate_input(value)

        assert success is False
        assert value == 1
        expected_error = 'Please enter a number between {} and {}'\
            .format(minimum, self.maximum)
        assert error == expected_error

    def test_upper_bounds(self):
        maximum = 50
        guess = '60'

        ng = NumberGuess(self.value, maximum=maximum)
        success, value, error = ng.validate_input(guess)

        assert success is False
        assert value == 60
        expected_error = 'Please enter a number between {} and {}'\
            .format(self.minimum, maximum)
        assert error == expected_error


class TestCheckGuess(unittest.TestCase):

    def test_equal(self):
        value = 10
        guess = 10

        ng = NumberGuess(value)
        keep_going, msg = ng.check_guess(guess)

        assert keep_going is False
        assert msg == "Congratulations the value was {}".format(value)

    def test_under(self):
        value = 50
        guess = 1

        ng = NumberGuess(value)
        keep_going, msg = ng.check_guess(guess)

        assert keep_going is True
        assert msg == 'Guess was too low.'

    def test_over(self):
        value = 50
        guess = 51

        ng = NumberGuess(value)
        keep_going, msg = ng.check_guess(guess)

        assert keep_going is True
        assert msg == 'Guess was too high.'


class MockNumberGuess(NumberGuess):

    def __init__(self, *args, **kwargs):
        super(MockNumberGuess, self).__init__(*args, **kwargs)

        self.completed = False
        self.displayed_correct = False
        self.error_count = 0

    def get_number(self):
        guess = '50'
        success, guess, error = self.validate_input(guess)
        return guess

    def display_error_message(self, msg):
        self.error_count += 1

    def display_success_message(self):
        self.completed = True

    def display_remaining_chances(self):
        pass

    def display_correct_value(self):
        self.displayed_correct = True


class TestGuessAmount(unittest.TestCase):

    def test_valid(self):
        value = 50
        mng = MockNumberGuess(value=value)
        mng.run_turn(50)

        assert mng.completed is True
        assert mng.displayed_correct is False
        assert mng.remaining_chances == 6

    def test_max_failures_reached(self):
        value = 10
        max_chances = 7

        mng = MockNumberGuess(value=value, max_chances=max_chances)

        # Want at least one more test than the max.
        for _ in range(max_chances + 1):
            mng.run_turn(50)

        assert mng.completed is False
        assert mng.remaining_chances == 0
        assert mng.error_count == max_chances
        assert mng.displayed_correct is True

    def test_correct_midway(self):
        max_chances = 7
        mng = MockNumberGuess(value=10, max_chances=max_chances)

        mng.run_turn(50)
        mng.run_turn(30)
        mng.run_turn(10)

        assert mng.completed is True
        assert mng.remaining_chances == 4
        assert mng.error_count == 2
        assert mng.displayed_correct is False

if __name__ == '__main__':
    unittest.main()
