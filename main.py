# Random number 0 to 100
# 7 tries to do this guess
# Only update chances if they use numbers.
# Tell the user if their guess was higher or lower.

import argparse
from random import randint

from game import ConsoleNumberGuess


def main():
    description = 'Play a number guessing game.'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--min', type=int, default=0,
                        help='The minimum value')
    parser.add_argument('--max', type=int, default=100,
                        help='The maximum value')
    parser.add_argument('--chances', type=int, default=7,
                        help='The maximum amount of guesses')

    args = parser.parse_args()

    value = randint(args.min, args.max)

    cng = ConsoleNumberGuess(value, minimum=args.min, maximum=args.max,
                             max_chances=args.chances)
    cng.run()

if __name__ == '__main__':
    main()
