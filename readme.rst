====================
Number Guessing Game
====================

Prompts for a number from the console and displays whether you guess correctly,
are too high, or are too low.

--------------
Python version
--------------

Tested on 2.7 and 3.4
